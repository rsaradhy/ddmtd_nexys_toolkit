# Written by Rohith Saradhy
# Email -> rohithsaradhy@gmail.com
#gcc -O program_fpga.c  -l bcm2835 -o ORMFlash.exe
# gcc -O program_fpga_Nex.c  -l bcm2835 -o NexFlash.exe
# gcc -O GBT_SCA.c  spi_common.c -l bcm2835 -o GBT_SCA.exe
# gcc -O sca_blink.c spi_common.c  -l bcm2835 -o sca_blink.exe

# gcc -O ddmtd_mem.c spi_common.c NexysDDMTD.c -l bcm2835 -o ddmtd_mem.exe
# echo Done Compiling MEM


gcc -O data_acq.c spi_common.c NexysDDMTD.c -l bcm2835 -o data_acq.exe
echo Done Compiling data_acq


gcc -O ddmtd_pll.c spi_common.c NexysDDMTD.c -l bcm2835 -o ddmtd_pll.exe
echo Done Compiling PLL


# gcc -O check_firmware.c spi_common.c NexysDDMTD.c -l bcm2835 -o check_firmware.exe
# echo Done Compiling CheckFirmware
