// # Written by Rohith Saradhy
// # Email -> rohithsaradhy@gmail.com
#include <bcm2835.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <inttypes.h>
#include "spi_common.h"
#include "NexysDDMTD.h"


const int  FIRMWARE_ADDR = 0x100;

int main(int argc, char** argv)
{
  // Startup the SPI interface on the Pi.
  init_spi();
  
  int output;
  output = spi_NexyDDMTD_io(FIRMWARE_ADDR,0x1);  // send to random address to get back the firmware
  fprintf(stderr,"FirmwareVersion::%03.3f\n",((float)(output))/1000);

  //End SPI
  end_spi();
}