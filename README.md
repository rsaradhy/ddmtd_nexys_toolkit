# DDMTD Toolkit
This is a library that can be used to acquire data using DDMTD-Nexys Board and analyze it.  Contact **rohith.saradhy@cern.ch** for more information.

---
> ## Instructions
>1. Install Python 3
>2. Install Virtualenv 
>3. Run the following commands in sequence:
>
>>``` bash
>>git clone https://gitlab.cern.ch/rsaradhy/ddmtd_nexys_toolkit.git 
>>cd ddmtd_nexys_toolkit/
>>source env_setup.sh
>>source ddmtd_env/bin/activate
>>cd Analysis/
>>jupyter-notebook
>>```
>4. After the Jupyter-notebook opens up in your browser,  open the notebook name **MeasurementWorkBook.ipynb**. Follow the cell by cell instructions there.


