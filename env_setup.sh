#!/bin/bash


echo "Checking the version of virtual environment, if this gives a error, please install virtual environment"
virtualenv --version
virtualenv ddmtd_env
source ddmtd_env/bin/activate
pip install -r requirements.backup
deactivate


echo "Please run 'source ddmtd_env/bin/activate' to activate the python environment"